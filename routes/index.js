var express = require('express');
var router = express.Router();
const mysql = require('mysql')

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});
router.get('/getTimesStories', function(req, res, next) {
  // res.render('index', { title: 'Express' });
  const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'store'
  })

  connection.connect(function(err) {
    if (err) {
      console.error('error connecting: ' + err.stack);
      return;
    }

    console.log('connected as id ' + connection.threadId);
  });

  connection.query('SELECT title,link FROM stories', (err, rows, fields) => {
    if (err) throw err
    res.json(rows);
  })

  connection.end()
});
module.exports = router;
